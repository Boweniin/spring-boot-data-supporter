package com.hrhx.springboot.mongodb.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.hrhx.springboot.domain.Cnarea;
/**
 * 
 * @author duhongming
 *
 */
public interface CnareaRepository extends MongoRepository<Cnarea, Integer> {
	/**
	 * 获取相应区域级别信息
	 * @param level
	 * @return
	 */
	List<Cnarea> findByLevel(Integer level);
	/**
	 * 根据父级别获取相应区域级别信息
	 * @param level
	 * @param id
	 * @return
	 */
	List<Cnarea> findByLevelAndParentId(Integer level,Integer id);
	
}
