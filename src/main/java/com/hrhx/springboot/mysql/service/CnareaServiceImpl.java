package com.hrhx.springboot.mysql.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.hrhx.springboot.domain.Cnarea;
/**
 * 
 * @author duhongming
 *
 */
@Service
public class CnareaServiceImpl implements CnareaService {
	
    @Autowired
    private JdbcTemplate jdbcTemplate;

	@Override
	public List<Cnarea> getAllFirstLevel() throws Exception{
		// TODO Auto-generated method stub
		try{			
			return jdbcTemplate.query("select * from cnarea where level=0",new BeanPropertyRowMapper<Cnarea>(Cnarea.class));
		}catch(Exception e){
			throw new Exception();
		}
	}

	@Override
	public List<Cnarea> getByParentId(Integer level,Integer id) throws Exception{
		// TODO Auto-generated method stub
		try{
			return jdbcTemplate.query("select * from cnarea where level=? and parent_id=?",new Object[]{level,id},new BeanPropertyRowMapper<Cnarea>(Cnarea.class));
		}catch(Exception e){
			throw new Exception();
		}
	}
    
 
}